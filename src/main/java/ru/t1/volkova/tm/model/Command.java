package ru.t1.volkova.tm.model;

import ru.t1.volkova.tm.constant.ArgumentConst;
import ru.t1.volkova.tm.constant.CommandConst;

public class Command {

    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show about program.");

    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show program version.");

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show list arguments.");

    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system information.");

    public static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    private String name;

    private String argument;

    private String desription;

    public Command() {
    }

    public Command(String name, String argument, String desription) {
        this.name = name;
        this.argument = argument;
        this.desription = desription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDesription() {
        return desription;
    }

    public void setDesription(String desription) {
        this.desription = desription;
    }

    @Override
    public String toString() {
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (desription != null && !desription.isEmpty()) displayName += ": " + desription;
        return displayName;
    }

}
